package com.example.cronjob.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CronJobController {
    private static final Logger logger = LoggerFactory.getLogger(CronJobController.class);

//    @Scheduled(fixedRate = 2000)
//    public void scheduleTaskWithFixedRate() {
//        logger.info("scheduleTaskWithFixedRate");
//    }
//
//    @Scheduled(fixedDelay = 10000)
//    public void scheduleTaskWithFixedDelay() {
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        logger.info("scheduleTaskWithFixedDelay");
//    }

    @Scheduled(fixedRate = 1000, initialDelay = 10000)
    public void scheduleTaskWithInitialDelay() {
        // do something
        logger.info("scheduleTaskWithInitialDelay");
    }
    public void scheduleTaskWithCronExpression() {
        // do something
    }
}
